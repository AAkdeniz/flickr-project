//
//  TableViewCell.swift
//  Ahmet_FinalExam_IO2
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

  
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    func update (with imageItem: ImageItem) {
        
        guard let url = imageItem.media.m!.withHTTPS() else {return}
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let data = data,
                
                //Convert received data to UIImage
                let image = UIImage(data: data) {
                
                //Dispatch to main Queue
                DispatchQueue.main.async {
                    
                    //Populate the UI
                    
                    self.dateLabel.text = imageItem.dateTaken
                    self.authorLabel.text  = imageItem.author
                    self.photo.image = image
                }
            }
        }
        task.resume()
    }
 
}
