//
//  Image.swift
//  Ahmet_FinalExam_IO2
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import Foundation
import UIKit

struct ImageItems: Codable {
    let items: [ImageItem]
    
}


struct Media: Codable {
    var m: URL?
 
}


struct ImageItem : Codable {

    var media: Media

    var author: String
    var dateTaken: String
    var description: String
    var published: String
    var tags: String


    enum CodingKeys: String, CodingKey {

        case media = "media"

        case author = "author"
        case dateTaken = "date_taken"
        case description = "description"
        case published = "published"
        case tags = "tags"
    }

    init(_ media: Media, _ author: String, _ dateTaken: String, _ description: String, _ published: String, tags: String) {

        self.media = media
        self.author = author
        self.dateTaken = dateTaken
        self.description = description
        self.published = published
        self.tags = tags

    }

    static let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    static let archiveURL = documentDirectory.appendingPathComponent("imageList").appendingPathExtension("aca")
    

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        author = try container.decode(String.self, forKey: .author)
        dateTaken = try container.decode(String.self, forKey: .dateTaken)
        description = try container.decode(String.self, forKey: .description)
        published = try container.decode(String.self, forKey: .published)
        tags = try container.decode(String.self, forKey: .tags)

        media = try container.decode(Media.self, forKey: .media)

    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(author, forKey: .author)
        try container.encode(dateTaken, forKey: .dateTaken)
        try container.encode(description, forKey: .description)
        try container.encode(published, forKey: .published)
        try container.encode(tags, forKey: .tags)

       try container.encode(media, forKey: .media)

    }
}


