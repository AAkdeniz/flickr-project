//
//  URL_Helper.swift
//  Ahmet_FinalExam_IO2
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import Foundation

extension URL {
    
    func withQueries(_ queries: [String:String]) -> URL? {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.queryItems = queries.compactMap { URLQueryItem(name: $0.0, value: $0.1)}
        
        return components?.url
    }
    // Convert http to https
    func withHTTPS() -> URL? {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.scheme = "https"
        
        return components?.url
    }
}
