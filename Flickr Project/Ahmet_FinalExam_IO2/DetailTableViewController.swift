//
//  DetailTableViewController.swift
//  Ahmet_FinalExam_IO2
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController {
    
    var imageItem: ImageItem?
    var detailImage: UIImage?

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateTakenLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let imageItem = imageItem {
            
            update(with: imageItem)
     
            
        }
    }
    
    
    
    func update (with imageItem: ImageItem) {
        
        guard let url = imageItem.media.m!.withHTTPS()  else {return}
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let data = data,
                
                //Convert received data to UIImage
                 let image = UIImage(data: data) {
                
                //Dispatch to main Queue
                DispatchQueue.main.async {
                    
                    //Populate the UI
                    
                    self.authorLabel.text = imageItem.author
                    self.dateTakenLabel.text  = imageItem.dateTaken
                    self.descriptionLabel.text = imageItem.description
                    self.publishedDateLabel.text = imageItem.published
                    self.tagsLabel.text = imageItem.tags
                    self.detailImage = image
                    self.photoImageView.image = image
                }
            }
        }
        task.resume()
    }
    
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        
       
            guard let image = detailImage else {return}
           
                let activityController = UIActivityViewController(activityItems: [image], applicationActivities: nil)
                
                activityController.popoverPresentationController?.sourceView = sender
                present(activityController, animated: true, completion: nil)
        
        
    }
    
    
    
    
}
