//
//  MainTableViewController.swift
//  Ahmet_FinalExam_IO2
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {
    
    var imageItems = [ImageItem]()
    
    var offlineList = [ImageItem]()
    
    var offline = false
    
    
    @IBOutlet weak var editButtonPosition: UIBarButtonItem!
    @IBOutlet weak var saveButtonPosition: UIBarButtonItem!
    
    let imageInfoController = ImageInfoController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        editButtonPosition.isEnabled = false
        saveButtonPosition.isEnabled = false
        
      if !offline {
        if let imageItem = load() {
            offlineList = imageItem
            }
      }
        
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        imageInfoController.fetchImages{ (imageItems) in
            if let imageItems = imageItems {
                self.updateUI(with: imageItems)
            }
        }
    }
    
    
    func updateUI(with imageItems: [ImageItem]){
        if !offline {
        self.imageItems = imageItems
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        } else {
            offlineList = imageItems
    
        }
        
    }
    
    @IBAction func saveButtonTapped(_ sender: UIBarButtonItem) {
     
        save()
    }
    
    @IBAction func segmentController(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            offline = false
            editButtonPosition.isEnabled = false
            saveButtonPosition.isEnabled = false
            save()
             self.tableView.reloadData()
        case 1:
            offline = true
            editButtonPosition.isEnabled = true
            saveButtonPosition.isEnabled = true
             self.tableView.reloadData()
       default:
            offline = false
            sender.isEnabledForSegment(at: 0)
            sender.isEnabledForSegment(at: 1)
        }
    }
    
    @IBAction func editButtonTapped(_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        
        save()
    }
    
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            offlineList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
            //Call save() function to save after deleting
            save()
            
            
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetail" {
            
            if !offline {
            
            let indexPath = tableView.indexPathForSelectedRow!
            let selectedSubject = imageItems[indexPath.row]
            let nVC = segue.destination as! UINavigationController
            let detailTableViewController = nVC.viewControllers.first as! DetailTableViewController
            
            
            detailTableViewController.imageItem = selectedSubject
            }
            
            else{
            let indexPath = tableView.indexPathForSelectedRow!
            let selectedSubject = offlineList[indexPath.row]
            let nVC = segue.destination as! UINavigationController
            let detailTableViewController = nVC.viewControllers.first as! DetailTableViewController
            
            
            detailTableViewController.imageItem = selectedSubject
            }
        }
    }
    
    
    @IBAction func unwindToSearchPage(_ unwindSegue: UIStoryboardSegue) {
  
    }

    
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !offline {
        return imageItems.count
        } else {
            return offlineList.count
        }
       
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        if !offline{
        let imageItem = imageItems[indexPath.row]
        cell.update(with: imageItem)
        
        
        return cell
        } else {
            let offlineItem = offlineList[indexPath.row]
            cell.update(with: offlineItem)
            return cell
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    //For Encode
    func save (){
        
        let propertyListEncoder = PropertyListEncoder()
        
        if offline{
        let encodedNoteArray = try? propertyListEncoder.encode(offlineList)
        try? encodedNoteArray?.write(to: ImageItem.archiveURL, options: .noFileProtection)
        }else{
            let encodedNoteArray = try? propertyListEncoder.encode(imageItems)
            try? encodedNoteArray?.write(to: ImageItem.archiveURL, options: .noFileProtection)
            
        }
    }
    
    
    //For Decode
    func load ()->[ImageItem]? {
          let propertyListDecoder = PropertyListDecoder()

        if let retrievedNotesData = try? Data(contentsOf: ImageItem.archiveURL), let decodedNotes = try? propertyListDecoder.decode(Array<ImageItem>.self, from: retrievedNotesData){
            return decodedNotes
        }else {
            return nil
        }
    }
}
