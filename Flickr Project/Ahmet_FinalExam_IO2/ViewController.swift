//
//  ViewController.swift
//  Ahmet_FinalExam_IO2
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var imageItems = [ImageItem]()
    let imageInfoController = ImageInfoController()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        set()
        print(imageItems)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func updateUI(with imageItems: [ImageItem]) {
        self.imageItems = imageItems
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
    func set() {
        
        let query: [String: String] = [
            "format": "json",
            "nojsoncallback": "1" ]
        
        //It will shows a spinner in status bar
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        imageInfoController.fetchImages(matching: query) { (imageList) in
            
            if let imageList = imageList {
                self.updateUI(with: imageList)
                
            }
        }
    }
    
}
    
    
extension ViewController: UITableViewDelegate, UITableViewDataSource {  // MARK: - Table view data source
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return imageItems.count
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        
        let imageItem = imageItems[indexPath.row]
        cell.update(with: imageItem)
        
        
        return cell
        
        
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    
}
