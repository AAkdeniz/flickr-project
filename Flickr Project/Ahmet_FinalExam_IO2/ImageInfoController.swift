//
//  ImageInfoController.swift
//  Ahmet_FinalExam_IO2
//
//  Created by mobileapps on 2019-02-19.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import Foundation

struct ImageInfoController {
    
    func fetchImages(completion: @escaping ([ImageItem]?) -> Void) {
        
        let baseURL = URL(string: "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1")!
        
        guard let url = baseURL.withHTTPS() else {
            completion(nil)
            print("Unable to build URL with supplied queries.")
            return
        }
        
        print(url)
        // Parallel Execution Path -----------------------------
        // Runnung this task will convert received data as bytes from web service to swift object
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            let decoder = JSONDecoder()
            if let data = data,
                let storeItems = try? decoder.decode(ImageItems.self, from: data) {
                completion(storeItems.items)
            } else {
                print("Either no data was returned, or data was not serialized.")
                completion(nil)
                return
            }
        }
        task.resume()
    } //------------------------------------------------------
}

